package com.sda.grupa1.com.sda.webstore.model.products;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private String id;
    private String name;
    private String manufacturer;
    private int price;
    private String description;
    private int onStock;
    
}
